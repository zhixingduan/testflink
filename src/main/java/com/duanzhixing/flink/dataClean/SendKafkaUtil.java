/**
 * 
 */
package com.duanzhixing.flink.dataClean;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.alibaba.fastjson.JSON;

/**
 * @author duanzhixing
 *
 *产生20条模拟数据
 */
public class SendKafkaUtil
{

    public static void main(String[] args) throws InterruptedException
    {
        Properties props = new Properties();//New configuration file
        props.put("bootstrap.servers", "node1:9092");//you should write specific ip address rather than localhost
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");//StringSerializer/IntegerSerializer/or other self-defined Serializer.
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        //the producer produce to  specified topic
        String topic = "person";
        Producer<String, String> procuder = new KafkaProducer<String, String>(props);
        for (int i = 1; i <= 20; i++)
        {
            Thread.currentThread().sleep(1000);
            Person p = new Person();
            p.setName("小明" + i);
            p.setAge(i);
            p.setDateTime(new Date().getTime());
            p.setTimes(1);
            if (i % 2 == 0)
            {
                p.setKey("k1");
            }
            else
            {
                p.setKey("k2");

            }
            String value = JSON.toJSONString(p);

            //produce message through ProducerRecord<string,String>
            ProducerRecord<String, String> msg = new ProducerRecord<String, String>(topic, value);
            procuder.send(msg);
        }

        for (int i = 1; i <= 20; i++)
        {
            Thread.currentThread().sleep(1000);
            Person p = new Person();
            p.setName("小明" + i);
            p.setAge(i);
            p.setDateTime(new Date().getTime());
            p.setTimes(1);

            if (i % 2 == 0)
            {
                p.setKey("k1");
            }
            else
            {
                p.setKey("k2");

            }
            String value = JSON.toJSONString(p);
            //produce message through ProducerRecord<string,String>
            ProducerRecord<String, String> msg = new ProducerRecord<String, String>(topic, value);
            procuder.send(msg);
        }

        System.out.println("send message over.");
        procuder.close();

    }
}
