/**
 * 
 */
package com.duanzhixing.flink.dataClean;

import java.util.Properties;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoFlatMapFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.util.Collector;

import com.alibaba.fastjson.JSON;

import redis.clients.jedis.Jedis;

/**
 * @author duanzhxing
 *
 *简单的数据清洗demo
 *
 *从mysql中获取清洗规则
 *从kafka中获取数据
 *
 *这里获取的是person中  times 大于规则中times的数据写出
 *
 *如果有关联查询，可以如map中的数据处理来进行创建连接，查询数据
 */
public class DataClean
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(3);

        Properties properties = new Properties();

        //开启checkpoint ,保存offset 和operator 信息
        env.enableCheckpointing(1000);

        //设置kafka地址
        properties.setProperty("bootstrap.servers", "node1:9092");
        // only required for Kafka 0.8
        properties.setProperty("zookeeper.connect", "node2:2181,node3:2181,node4:2181");
        properties.setProperty("group.id", "test");
        //这里kafka接收到的数据是string 所以泛型写string,FlinkKafkaConsumer 需要跟kafka版本一致
        //        DataStreamSource<String> addSource = env.addSource(new FlinkKafkaConsumer<String>("topic1", new SimpleStringSchema(), properties));
        FlinkKafkaConsumer011<String> myKafka = new FlinkKafkaConsumer011<String>("person", new SimpleStringSchema(), properties);

        myKafka.setStartFromGroupOffsets();

        DataStreamSource<Rule> setParallelism = env.addSource(new MyRule()).setParallelism(1);
        DataStream<Rule> broadcast = setParallelism.broadcast();
        DataStreamSource<String> addSource = env.addSource(myKafka);

        SingleOutputStreamOperator<Person> map = addSource.map(new RichMapFunction<String, Person>()
        {

            private static final long serialVersionUID = 1L;
            private Jedis jedis;

            @Override
            public void open(Configuration parameters) throws Exception
            {
                if (jedis == null)
                {
                    jedis = new Jedis("node1", 6379);

                }
            }

            @Override
            public Person map(String arg0) throws Exception
            {
                Person p = JSON.parseObject(arg0, Person.class);

                System.out.println("当前jedis为--" + jedis.toString() + "--当前线程为--" + Thread.currentThread().getId() + "当前jedis查询结果为" + jedis.get(p.getKey()));

                return p;
            }

            @Override
            public void close() throws Exception
            {
                if (jedis != null)
                {
                    jedis.close();
                }
            }
        });

        SingleOutputStreamOperator<Person> reduce = map.keyBy("name").reduce(new ReduceFunction<Person>()
        {

            Person p = new Person();

            @Override
            public Person reduce(Person arg0, Person arg1) throws Exception
            {
                p.setName(arg0.getName());
                p.setDateTime(arg1.getDateTime());
                p.setAge(arg1.getAge());
                p.setTimes(arg0.getTimes() + arg0.getTimes());
                return p;
            }
        });

        SingleOutputStreamOperator<Person> flatMap = broadcast.connect(reduce).flatMap(new CoFlatMapFunction<Rule, Person, Person>()
        {

            Rule rule = new Rule();

            @Override
            public void flatMap1(Rule value, Collector<Person> out) throws Exception
            {

                rule = value;

            }

            @Override
            public void flatMap2(Person value, Collector<Person> out) throws Exception
            {
                System.out.println("当前线程为" + Thread.currentThread().getId() + "------当期规则为------" + JSON.toJSON(rule));
                if (value.getTimes() > rule.getAlltimes())
                {
                    out.collect(value);
                }

            }
        });

        flatMap.print().setParallelism(1);

        env.execute();
    }
}
