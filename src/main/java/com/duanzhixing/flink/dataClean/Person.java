/**
 * 
 */
package com.duanzhixing.flink.dataClean;

import java.io.Serializable;

/**
 * @author Administrator
 *
 */
public class Person implements Serializable
{

    private static final long serialVersionUID = 1L;

    //名字
    private String name;

    //年龄
    private int age;

    //连接次数
    private int times;

    //连接时间
    private Long DateTime;

    private String key;

    /**
     * @return the key
     */
    public String getKey()
    {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key)
    {
        this.key = key;
    }

    /**
     * @return the times
     */
    public int getTimes()
    {
        return times;
    }

    /**
     * @param times the times to set
     */
    public void setTimes(int times)
    {
        this.times = times;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge()
    {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age)
    {
        this.age = age;
    }

    /**
     * @return the dateTime
     */
    public Long getDateTime()
    {
        return DateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Long dateTime)
    {
        DateTime = dateTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "Person [name=" + name + ", age=" + age + ", times=" + times + ", DateTime=" + DateTime + "]";
    }

}
