/**
 * 
 */
package com.duanzhixing.flink.dataClean;

import java.io.Serializable;

/**
 * @author Administrator
 *规则，
 *
 */
public class Rule implements Serializable
{

    private static final long serialVersionUID = 1L;

    //要求times大于数字
    private int alltimes;

    private String name;

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the alltimes
     */
    public int getAlltimes()
    {
        return alltimes;
    }

    /**
     * @param alltimes the alltimes to set
     */
    public void setAlltimes(int alltimes)
    {
        this.alltimes = alltimes;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "Rule [alltimes=" + alltimes + ", name=" + name + "]";
    }

}
