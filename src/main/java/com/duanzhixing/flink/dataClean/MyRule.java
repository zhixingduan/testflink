/**
 * 
 */
package com.duanzhixing.flink.dataClean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

/**
 * @author Administrator
 *
 */
public class MyRule extends RichSourceFunction<Rule>
{

    private static Log log = LogFactory.getLog(MyRule.class);

    private static final long serialVersionUID = 1L;
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;

    @Override
    public void open(Configuration parameters) throws Exception
    {
        Class.forName("com.mysql.jdbc.Driver");
        System.out.println("成功加载驱动");
        String url = "jdbc:mysql://node1:3306/test?user=root&password=123&useUnicode=true&characterEncoding=UTF8";
        connection = DriverManager.getConnection(url);
        System.out.println("成功获取连接");
    }

    @Override
    public void run(org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext<Rule> ctx) throws Exception
    {
        Rule rule = new Rule();
        PreparedStatement pstmt;

        String sql = "SELECT * FROM rule";
        pstmt = connection.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next())
        {
            rule.setAlltimes(rs.getInt("times"));
            ctx.collect(rule);
        }

    }

    /* (non-Javadoc)
     * @see org.apache.flink.streaming.api.functions.source.SourceFunction#cancel()
     */
    @Override
    public void cancel()
    {
        try
        {
            if (resultSet != null)
            {
                resultSet.close();
            }
            if (statement != null)
            {
                statement.close();
            }
            if (connection != null)
            {
                connection.close();
            }
        }
        catch (Exception e)
        {
            log.error(e);
        }

        System.out.println("成功关闭资源");

    }

}
