package com.duanzhixing.flink.sql;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

import com.duanzhixing.flink.test.SensorReading;

/**
 * @author duanzhixing
 *
 *从流中转换table
 */
public class TestTableFromStream_2
{

    public static void main(String[] args) throws Exception
    {

        //1、创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamEnv = StreamTableEnvironment.create(env);

        DataStreamSource<String> readTextFile = env.readTextFile("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt");
        SingleOutputStreamOperator<SensorReading> map = readTextFile.map(new MapFunction<String, SensorReading>()
        {
            @Override
            public SensorReading map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SensorReading(split[0], Long.valueOf(split[1]), Double.valueOf(split[2]));
            }
        });
        Table from = streamEnv.fromDataStream(map);

        //3、查询聚合操作
        Table filter = from.select("id,temp").filter("id='设备1'");
        //或者用sql进行聚合查询
        Table sqlQuery = streamEnv.sqlQuery("select id, count(id) as cnt, avg(temp) as avgTemp from inputTable group by id");

        //4、输出到控制台
        streamEnv.toAppendStream(filter, Row.class).print("result");
        streamEnv.toRetractStream(sqlQuery, Row.class).print("sqlQuery");

        env.execute();

    }
}
