package com.duanzhixing.flink.sql;

public class UserNetFlow
{

    private String userId;
    private String ruleId;

    public UserNetFlow()
    {
        super();
    }

    public UserNetFlow(String userId, String ruleId)
    {
        super();
        this.userId = userId;
        this.ruleId = ruleId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getRuleId()
    {
        return ruleId;
    }

    public void setRuleId(String ruleId)
    {
        this.ruleId = ruleId;
    }

}
