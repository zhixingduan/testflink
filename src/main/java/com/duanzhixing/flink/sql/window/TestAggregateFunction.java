package com.duanzhixing.flink.sql.window;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.table.functions.AggregateFunction;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *验证测试table 中聚合函数的写法
 *
 *例如我们这里计算平均温度
 */
public class TestAggregateFunction
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        tableEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\sensor.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");

        //注册一个AggregateFunction
        MyAvg myAvg = new MyAvg();
        tableEnv.registerFunction("myAvg", myAvg);

        //测试table的使用
        Table from = tableEnv.from("inputTable");
        Table tableScalar = from.groupBy("id").aggregate("myAvg(temp) as myAvgtemp").select("id,myAvgtemp");
        tableEnv.toRetractStream(tableScalar, Row.class).print("tableScalar");

        //测试sql 的使用方法
        Table sqlQuery = tableEnv.sqlQuery("select id,myAvg(temp) from inputTable group by id");
        tableEnv.toRetractStream(sqlQuery, Row.class).print("sqlQuery");

        env.execute();
    }

    /**
     * @author duanzhixing
     *AggregateFunction 两个泛型参数，第一个是指传递的值，第二个是累加器
     */
    public static class MyAvg extends AggregateFunction<Double, Tuple2<Double, Integer>>
    {

        /**
         * 进行累加处理
         * @param temp
         * @param acc
         */
        public void accumulate(Tuple2<Double, Integer> acc, Double temp)
        {
            acc.f0 += temp;
            acc.f1 += 1;
        }

        /**
         *调用该方法返回计算结果
         *总温度除以总个数
         */
        @Override
        public Double getValue(Tuple2<Double, Integer> accumulator)
        {
            return accumulator.f0 / accumulator.f1;
        }

        /**
         *初始化一个累加器
         */
        @Override
        public Tuple2<Double, Integer> createAccumulator()
        {
            return new Tuple2<Double, Integer>(0.0, 0);
        }

    }
}
