package com.duanzhixing.flink.sql.window;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.table.functions.TableAggregateFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *测试验证表格聚合函数
 */
public class TestTableAggregateFunction
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        tableEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\sensor.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");

        //注册一个AggregateFunction
        MyTopTemp3 myTopTemp3 = new MyTopTemp3();
        tableEnv.registerFunction("myTopTemp", myTopTemp3);

        //测试table的使用
        Table from = tableEnv.from("inputTable");
        Table tableScalar = from.groupBy("id").flatAggregate("myTopTemp(temp) as myTopTemp3temp").select("id,myTopTemp3temp");
        tableEnv.toRetractStream(tableScalar, Row.class).print("tableScalar");

        //测试sql 的使用方法,目前在1.10 和1.11版本中这样写sql flink会报错
        //        Table sqlQuery = tableEnv.sqlQuery("select id,myTopTemp(temp) as myTopTemp3 from inputTable group by id");
        //        tableEnv.toRetractStream(sqlQuery, Row.class).print("sqlQuery");

        env.execute();
    }

    public static class MyTopTemp3 extends TableAggregateFunction<Double, List<Double>>
    {

        @Override
        public List<Double> createAccumulator()
        {
            return new ArrayList<Double>();
        }

        /**
         * 进行排序处理
         * @param temp
         * @param acc
         */
        public void accumulate(List<Double> acc, Double data)
        {
            acc.add(data);
            acc.sort(new Comparator<Double>()
            {
                @Override
                public int compare(Double o1, Double o2)
                {
                    return (int) (o2 * 100) - (int) (o1 * 100);
                }
            });

            System.out.println(acc.toString());
        }

        public void emitValue(List<Double> acc, Collector<Double> out)
        {
            int size = 3;
            if (acc.size() < 3)
            {
                size = acc.size();
            }
            for (int i = 0; i < size; i++)
            {
                out.collect(acc.get(i));
            }
        }

    }
}
