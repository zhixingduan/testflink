package com.duanzhixing.flink.sql.window;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.table.functions.ScalarFunction;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *测试标量函数
 *如下我们输入数据的id，想要得到该数据的id的hash值的一个方法。
 */
public class TestScalarFunction
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        tableEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\sensor.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");

        //注册一个scalarFunction
        MyHashCode myHashCode = new MyHashCode();
        tableEnv.registerFunction("MyHashCode", myHashCode);

        //测试table的使用
        Table from = tableEnv.from("inputTable");
        Table tableScalar = from.select("id,temp,MyHashCode(id)");
        tableEnv.toAppendStream(tableScalar, Row.class).print("tableScalar");

        //测试sql 的使用方法
        Table sqlQuery = tableEnv.sqlQuery("select id,temp,MyHashCode(id) from inputTable");
        tableEnv.toAppendStream(sqlQuery, Row.class).print("sqlQuery");

        env.execute();
    }

    /**
     * @author duanzhixing
     *自定义获取id和hashcode *2
     */
    public static class MyHashCode extends ScalarFunction
    {

        public int eval(String str)
        {
            return str.hashCode() * 2;

        }
    }
}
