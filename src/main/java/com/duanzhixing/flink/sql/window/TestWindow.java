package com.duanzhixing.flink.sql.window;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.table.api.Over;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.Tumble;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

import com.duanzhixing.flink.test.SensorReading;

/**
 * @author duanzhixing
 *测验证flink中table的窗口方法的使用
 */
public class TestWindow
{

    public static void main(String[] args) throws Exception
    {
        //1、创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //2、指定数据源获取表
        DataStreamSource<String> readTextFile = env.readTextFile("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\sensor.txt");
        SingleOutputStreamOperator<SensorReading> map = readTextFile.map(new MapFunction<String, SensorReading>()
        {
            @Override
            public SensorReading map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SensorReading(split[0], Long.valueOf(split[1]), Double.valueOf(split[2]));
            }
        }).assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2))
        {
            @Override
            public long extractTimestamp(SensorReading element)
            {
                return element.getTime() * 1000L;
            }
        });

        Table from = tableEnv.fromDataStream(map, "id, time as ts, temperature as temp, rt.rowtime");
        tableEnv.createTemporaryView("inputTable", from);

        System.out.println(from.getSchema());

        //3、测试Table开窗函数使用
        Table resultTable = from.window(Tumble.over("10.seconds").on("rt").as("tw"))
                //分组
                .groupBy("id,tw")
                //查询下10s内，id分组后的个数，平均温度，窗口结束时间
                .select("id ,id.count,temp.avg,tw.end");

        //4、测试下sql的写法
        Table resultSql = tableEnv.sqlQuery("select id, count(id) as cnt, avg(temp) as avgTemp, tumble_end(rt, interval '10' second) " + "from inputTable group by id, tumble(rt, interval '10' second)");

        tableEnv.toRetractStream(resultTable, Row.class).print("resultTable");
        tableEnv.toRetractStream(resultSql, Row.class).print("resultSql");

        //5测试下over窗口Table api
        Table overTable = from.window(Over.partitionBy("id").orderBy("rt").preceding("2.rows").as("ow"))
                //根据ow窗口查询如下数据聚合
                .select("id,rt,id.count over ow,temp.avg over ow");
        tableEnv.toRetractStream(overTable, Row.class).print("overTable");

        //6测试使用over 的sql
        Table overSql = tableEnv.sqlQuery("select id,rt,count(id) over ow,avg(temp) over ow from inputTable window ow as(partition by id order by rt rows between 2 preceding and current row)");
        tableEnv.toRetractStream(overSql, Row.class).print("overSql");

        env.execute();
    }
}
