package com.duanzhixing.flink.sql.window;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *测试Table function 
 *如下是我们输入一条数据后，将数据id按照_ 进行分割后，炸裂出多条数据。
 */
public class TestTableFunction
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        tableEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\sensor.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");

        //注册一个tableFunction
        MySplit mySplit = new MySplit();
        tableEnv.registerFunction("mySplit", mySplit);

        //测试table的使用,TableFunction 炸裂出来的数据可以做为一个表来看待，这里将炸裂出来的数据做联接输出
        Table tableScalar = tableEnv.from("inputTable").joinLateral("mySplit(id) as (word,length)").select("id,temp,word,length");
        tableEnv.toAppendStream(tableScalar, Row.class).print("tableScalar");

        //测试sql 的使用方法
        Table sqlQuery = tableEnv.sqlQuery("select id,temp,word,length from inputTable,lateral table(mySplit(id)) as mySplit(word, length)");
        tableEnv.toAppendStream(sqlQuery, Row.class).print("sqlQuery");

        env.execute();
    }

    public static class MySplit extends TableFunction<Tuple2<String, Integer>>
    {

        public void eval(String id)
        {
            String[] split = id.split("_");
            for (String word : split)
            {
                collector.collect(new Tuple2<String, Integer>(word, split.length));
            }
        }
    }
}
