package com.duanzhixing.flink.sql;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *测试Table的输出，如果是增量输出的streamEnv
 */
public class TestTableSink_3
{

    public static void main(String[] args) throws Exception
    {

        //1、创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamEnv = StreamTableEnvironment.create(env);

        //2、获取数据源
        streamEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");
        Table fromtable = streamEnv.from("inputTable");

        //3、查询聚合操作
        Table filter = fromtable.select("id,temp").filter("id='设备1'");
        //或者用sql进行查询
        Table sqlQuery = streamEnv.sqlQuery("select id,temp from inputTable where id='设备1'");

        //4.最后结果输出
        //4、1输出到控制台
        streamEnv.toAppendStream(filter, Row.class).print("result");
        streamEnv.toRetractStream(sqlQuery, Row.class).print("sqlQuery");

        //4.2输出到文件中,注册一个输出表
        streamEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\result.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("outputTable");
        filter.insertInto("outputTable");
        env.execute();

    }
}
