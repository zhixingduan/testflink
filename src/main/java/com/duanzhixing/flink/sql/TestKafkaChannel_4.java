package com.duanzhixing.flink.sql;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Kafka;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *
 *测试将kafka作为flink的双端传输通道
 */
public class TestKafkaChannel_4
{

    public static void main(String[] args) throws Exception
    {
        //注册环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamenv = StreamTableEnvironment.create(env);

        //连接kafka
        streamenv.connect(new Kafka()
                //选择kafka版本
                .version("0.11")
                //主题
                .topic("sensor_input")
                //zk集群
                .property("zookeeper.connect", "192.168.40.102:2181")
                //kafka集群
                .property("bootstrap.servers", "192.168.40.102:9092")
                //设置分组
                .property("group.id", "testGroup"))
                //数据切分格式
                .withFormat(new Csv())
                //输入表的格式
                .withSchema(new Schema()
                        //id
                        .field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("kafkaInputTable");

        //注册表
        Table from = streamenv.from("kafkaInputTable");
        streamenv.toAppendStream(from, Row.class).print("input");

        Table where = from.select("id,temp").where("id='sensor_1'");

        streamenv.connect(new Kafka()
                //选择kafka版本
                .version("0.11")
                //主题
                .topic("sensor_out")
                //zk集群
                .property("zookeeper.connect", "192.168.40.102:2181,192.168.40.103:2181,192.168.40.104:2181")
                //kafka集群
                .property("bootstrap.servers", "192.168.40.102:9092,192.168.40.103:9092,192.168.40.104:9092")
                //设置分组
                .property("group.id", "testGroup"))
                //数据切分格式
                .withFormat(new Csv())
                //输入表的格式
                .withSchema(new Schema()
                        //id
                        .field("id", DataTypes.STRING()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("kafkaOutputTable");

        where.insertInto("kafkaOutputTable");

        streamenv.toAppendStream(where, Row.class).print("output");

        env.execute();

    }
}
