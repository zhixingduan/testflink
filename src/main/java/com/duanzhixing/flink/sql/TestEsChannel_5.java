package com.duanzhixing.flink.sql;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Elasticsearch;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Json;
import org.apache.flink.table.descriptors.Schema;

/**
 * @author duanzhixing
 *测试Table Elasticsearch
 */
public class TestEsChannel_5
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamenv = StreamTableEnvironment.create(env);

        //注册数据输入源table
        //2、获取数据源
        streamenv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");
        //        Table fromtable = streamenv.from("inputTable");

        Table sqlQuery = streamenv.sqlQuery("select id, count(id) as cnt, avg(temp) as avgTemp from inputTable group by id");

        //注册数据输入源table
        streamenv.connect(new Elasticsearch()
                //版本
                .version("6")
                //连接信息
                .host("192.168.40.132", 9200, "http")
                //索引
                .index("sensor").documentType("flinkOutTable"))
                //数据格式
                .withFormat(new Json()).withSchema(new Schema().field("id", DataTypes.STRING()).field("cnt", DataTypes.BIGINT()).field("avgTemp", DataTypes.DOUBLE())).inUpsertMode().createTemporaryTable("flinkOutTable");

        sqlQuery.insertInto("flinkOutTable");
        env.execute();
    }
}
