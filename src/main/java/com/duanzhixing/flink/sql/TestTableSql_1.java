package com.duanzhixing.flink.sql;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *
 *连接外部的系统
 *
 *1、测试Table 的Api 和Sql的Api
 *2、flink的Table的使用步骤
 *  0)引入pom依赖
 *  1）创建
 *  2）查询聚合操作
 *  3）最后的结果输出
 */
public class TestTableSql_1
{

    public static void main(String[] args) throws Exception
    {

        //1、创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamEnv = StreamTableEnvironment.create(env);

        //2、获取数据源
        //2.1从文件中获取流数据源

        //2.1.1获取文件地址
        streamEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt")).withFormat(new Csv()).withSchema(new Schema().field("id", DataTypes.STRING()).field("time", DataTypes.BIGINT()).field("temp", DataTypes.DOUBLE())).createTemporaryTable("inputTable");
        Table from = streamEnv.from("inputTable");

        //3、查询聚合操作
        Table filter = from.select("id,temp").filter("id='设备1'");
        //或者用sql进行查询
        Table sqlQuery = streamEnv.sqlQuery("select id,temp from inputTable where id='设备1'");

        //4.最后结果输出
        //4、1输出到控制台
        streamEnv.toAppendStream(filter, Row.class).print("result");
        streamEnv.toAppendStream(sqlQuery, Row.class).print("sqlQuery");

        env.execute();

    }
}
