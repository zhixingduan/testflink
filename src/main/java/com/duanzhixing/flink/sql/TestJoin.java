package com.duanzhixing.flink.sql;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

public class TestJoin
{

    public static void main(String[] args) throws Exception
    {

        //1、创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment streamEnv = StreamTableEnvironment.create(env);

        //2、获取数据源
        //2.1从文件中获取流数据源

        //2.1.1获取文件地址
        streamEnv.connect(new FileSystem().path("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\rule.txt")).withFormat(new Csv()).withSchema(new Schema()//
                .field("id", DataTypes.STRING())//
                .field("min", DataTypes.STRING())//
                .field("max", DataTypes.STRING())//
        ).createTemporaryTable("rule");

        DataStreamSource<String> stream = env.socketTextStream("localhost", 7777);
        SingleOutputStreamOperator<UserNetFlow> map = stream.map(line -> {
            String[] split = line.split(" ");
            return new UserNetFlow(split[0], split[1]);
        });
        streamEnv.createTemporaryView("userNet", map);

        Table sqlQuery2 = streamEnv.sqlQuery("select * " + "from userNet " + "left join rule on rule.id=userNet.ruleId");

        streamEnv.toRetractStream(sqlQuery2, Row.class).print("sqlQuery");

        env.execute();

    }
}
