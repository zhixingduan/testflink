/**
 * 
 */
package com.duanzhixing.flink.stream.sink;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

/**
 * @author duanzhixing
 *自定义Sink输出源
 *向sql库中插入数据
 */
public class MySqlSink extends RichSinkFunction<String>
{

    private static final long serialVersionUID = 1L;
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;

    @Override
    public void open(Configuration parameters) throws Exception
    {
        Class.forName("com.mysql.jdbc.Driver");
        System.out.println("成功加载驱动");
        String url = "jdbc:mysql://node1:3306/test?user=root&password=123&useUnicode=true&characterEncoding=UTF8";
        connection = DriverManager.getConnection(url);
        System.out.println("成功获取连接");
    }

    @Override
    public void invoke(String value) throws Exception
    {

        PreparedStatement pstmt;

        String sql = "insert into person (name) values('" + value + "')";
        pstmt = connection.prepareStatement(sql);
        pstmt.executeUpdate(sql);//执行sql语句
        System.out.println("成功操作数据库");

    }

    @Override
    public void close() throws Exception
    {
        if (resultSet != null)
        {
            resultSet.close();
        }
        if (statement != null)
        {
            statement.close();
        }
        if (connection != null)
        {
            connection.close();
        }
        System.out.println("成功关闭资源");
    }

}
