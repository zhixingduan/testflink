/**
 * 
 */
package com.duanzhixing.flink.stream.sink;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import com.duanzhixing.flink.stream.source.MySourcePara;

/**
 * @author Administrator
 *
 *使用自定义sink数据源，这里用MySql
 *
 */
public class TestMySink
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> addSource = env.addSource(new MySourcePara());

        SingleOutputStreamOperator<String> map = addSource.map(new MapFunction<String, String>()
        {

            @Override
            public String map(String value) throws Exception
            {
                System.out.println(value);
                return value + "hahha";
            }
        });

        map.addSink(new MySqlSink()).setParallelism(1);

        env.execute();

    }
}
