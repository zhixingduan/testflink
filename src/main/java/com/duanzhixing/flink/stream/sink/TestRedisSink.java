/**
 * 
 */
package com.duanzhixing.flink.stream.sink;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;

import com.duanzhixing.flink.stream.source.MySourcePara;

/**
 * @author duanzhixing
 *
 */
public class TestRedisSink
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> addSource = env.addSource(new MySourcePara());

        //将输入的单词组装成k-v格式的redis数据，key统一为l_word
        SingleOutputStreamOperator<Tuple2<String, String>> map = addSource.map(new MapFunction<String, Tuple2<String, String>>()
        {

            @Override
            public Tuple2<String, String> map(String value) throws Exception
            {

                return new Tuple2<>("l_word", value);
            }
        });

        //这里需要添加redis的依赖，flink-connector-redis_2.11,commons-pool2

        //创建redis的配置
        FlinkJedisPoolConfig redisConf = new FlinkJedisPoolConfig.Builder().setHost("192.168.40.132").setPort(6379).build();
        //实现一个redisMapper
        DataStreamSink<Tuple2<String, String>> addSink = map.addSink(new RedisSink<>(redisConf, new RedisMapper<Tuple2<String, String>>()
        {

            @Override
            public RedisCommandDescription getCommandDescription()
            {
                //这里存放对redis做怎样的新增操作，是hset还是add,还是zadd
                return new RedisCommandDescription(RedisCommand.LPUSH);
            }

            //从接收的数据中获取要操作的key
            @Override
            public String getKeyFromData(Tuple2<String, String> data)
            {
                return data.f0;
            }

            //从接收的数据中获取要操作的value
            @Override
            public String getValueFromData(Tuple2<String, String> data)
            {
                return data.f1;
            }
        }));

        env.execute();
    }
}
