package com.duanzhixing.flink.stream.sink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.elasticsearch.ElasticsearchSinkFunction;
import org.apache.flink.streaming.connectors.elasticsearch.RequestIndexer;
import org.apache.flink.streaming.connectors.elasticsearch6.ElasticsearchSink;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Requests;

import com.duanzhixing.flink.test.SensorReading;

public class TestEsSink
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> readTextFile = env.readTextFile("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt");
        SingleOutputStreamOperator<SensorReading> map = readTextFile.map(new MapFunction<String, SensorReading>()
        {

            @Override
            public SensorReading map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SensorReading(split[0], Long.valueOf(split[1]), Double.valueOf(split[2]));
            }
        });

        List<HttpHost> httpHosts = new ArrayList<HttpHost>();
        HttpHost httpHost = new HttpHost("192.168.40.132", 9200);
        httpHosts.add(httpHost);
        map.addSink(new ElasticsearchSink.Builder<SensorReading>(httpHosts, new MyEsSinkFunction()).build());

        env.execute();
    }

    public static class MyEsSinkFunction implements ElasticsearchSinkFunction<SensorReading>
    {

        private static final long serialVersionUID = 1L;

        @Override
        public void process(SensorReading element, RuntimeContext ctx, RequestIndexer indexer)
        {

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("id", element.getId());
            map.put("time", element.getTime());
            map.put("temperature", element.getTemperature());

            //构建index请求
            IndexRequest indexRequest = Requests.indexRequest();
            indexRequest.index("testflink");
            indexRequest.type("sencor_temp");
            indexRequest.source(map);

            indexer.add(indexRequest);
        }

    }
}
