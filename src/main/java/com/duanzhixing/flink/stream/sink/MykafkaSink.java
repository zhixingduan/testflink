package com.duanzhixing.flink.stream.sink;

import java.util.Arrays;
import java.util.Properties;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.kafka.clients.producer.ProducerConfig;

public class MykafkaSink
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> fromCollection = env.fromCollection(Arrays.asList("hello", "flink", "zhixing"));
        Properties producerConfig = new Properties();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.40.101:9092");
        FlinkKafkaProducer011<String> sink = new FlinkKafkaProducer011<String>("testdata", new SimpleStringSchema(), producerConfig);
        fromCollection.addSink(sink);
        env.execute();
    }
}
