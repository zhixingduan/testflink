/**
 * 
 */
package com.duanzhixing.flink.stream.partition;

import org.apache.flink.api.common.functions.Partitioner;

/**
 * @author duanzhixing
 *
 *自定义分区
 */
public class MyPartition implements Partitioner<String>
{

    /* 
     * 自定义分区规则，这里按照key的具体值进行分区
     */
    @Override
    public int partition(String key, int numPartitions)
    {

        if (key.equals("duan"))
        {
            return 1;
        }
        return 0;
    }

}
