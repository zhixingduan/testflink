/**
 * 
 */
package com.duanzhixing.flink.stream.partition;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import com.duanzhixing.flink.stream.source.MySourcePara;

/**
 * @author duanzhixing
 *测试自定义分区
 *
 */
public class TestPartition
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> addSource = env.addSource(new MySourcePara());
        //对单词进行数据转换变成tuple，否则后面使用自定义分区会有问题
        SingleOutputStreamOperator<Tuple1<String>> map = addSource.map(new MapFunction<String, Tuple1<String>>()
        {

            @Override
            public Tuple1<String> map(String value) throws Exception
            {
                return new Tuple1<>(value);
            }
        });

        //这里后面的0指的是对tuple1字段作为key进行分区处理
        DataStream<Tuple1<String>> partitionCustom = map.partitionCustom(new MyPartition(), 0);

        partitionCustom.map(new MapFunction<Tuple1<String>, String>()
        {

            @Override
            public String map(Tuple1<String> value) throws Exception
            {
                System.out.println("获取当前分区信息" + Thread.currentThread().getName() + "当前数据为：" + value.f0);
                return value.f0;
            }
        });

        env.execute();
    }
}
