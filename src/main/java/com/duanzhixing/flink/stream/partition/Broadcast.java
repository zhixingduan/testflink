/**
 * 
 */
package com.duanzhixing.flink.stream.partition;

import java.util.LinkedList;
import java.util.List;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author duanzhixing
 *
 *Broadcast 广播数据流到分区。将数据流广播给每一个分区去执行
 */
public class Broadcast
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(4);

        List<String> data = new LinkedList<>();
        data.add("duanzhixing");
        //读取数据时候设置分区是1
        DataStreamSource<String> addSource = env.fromCollection(data).setParallelism(1);

        //        addSource.map(new MapFunction<String, String>()
        //        {
        //
        //            @Override
        //            public String map(String value) throws Exception
        //            {
        //                System.out.println("当前数据所在分区信息" + Thread.currentThread().getName());
        //                System.out.println(value);
        //                return value;
        //            }
        //        }).print();

        //区分一下与上面没有广播时候并行度设置
        DataStream<String> broadcast = addSource.broadcast();
        broadcast.map(new MapFunction<String, String>()
        {

            @Override
            public String map(String value) throws Exception
            {
                System.out.println("当前数据所在分区信息" + Thread.currentThread().getName());
                System.out.println(value);
                return value;
            }
        }).print();
        env.execute();

    }
}
