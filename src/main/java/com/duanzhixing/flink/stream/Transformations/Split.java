/**
 * 
 */
package com.duanzhixing.flink.stream.Transformations;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.SplitStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

/**
 * @author duanzhixing
 *
 *split可以根据自己的规则将一个数据流切分成多个数据流
 */
public class Split
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<Object> addSource = env.addSource(new SourceFunction<Object>()
        {

            @Override
            public void run(org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext<Object> ctx) throws Exception
            {

                for (int i = 0; i < 10; i++)
                {
                    Thread.currentThread().sleep(1000);

                    if (i % 3 == 0)
                    {
                        ctx.collect("呵呵呵哒");

                    }
                    else
                    {
                        ctx.collect(i);

                    }
                }

            }

            @Override
            public void cancel()
            {
                // TODO Auto-generated method stub

            }

        });

        SplitStream<Object> split = addSource.split(new OutputSelector<Object>()
        {

            @Override
            public Iterable<String> select(Object value)
            {
                ArrayList<String> list = new ArrayList<String>();

                try
                {
                    new BigDecimal(value.toString());
                    list.add("是数字");
                }
                catch (Exception e)
                {
                    list.add("不是数字");
                }
                return list;
            }
        });

        DataStream<Object> select = split.select("不是数字");

        SingleOutputStreamOperator<String> map = select.map(new MapFunction<Object, String>()
        {

            @Override
            public String map(Object value) throws Exception
            {
                // TODO Auto-generated method stub
                return value.toString();
            }
        });

        map.print().setParallelism(1);
        env.execute();
    }
}
