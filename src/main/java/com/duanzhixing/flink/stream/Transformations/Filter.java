/**
 * 
 */
package com.duanzhixing.flink.stream.Transformations;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

import com.duanzhixing.flink.stream.source.MySourcePara;

/**
 * @author 过滤算子
 *map：输入一个元素，然后返回一个元素，中间可以做一些清洗转换等操作
flatmap：输入一个元素，可以返回零个，一个或者多个元素
filter：过滤函数，对传入的数据进行判断，符合条件的数据会被留下
keyBy：根据指定的key进行分组，相同key的数据会进入同一个分区【典型用法见备注】
reduce：对数据进行聚合操作，结合当前元素和上一次reduce返回的值进行聚合操作，然后返回一个新的值
aggregations：sum(),min(),max()等

 *
 */
public class Filter
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> fromCollection = env.addSource(new MySourcePara());

        //过滤算子
        SingleOutputStreamOperator<String> filter = fromCollection.filter(new FilterFunction<String>()
        {

            @Override
            public boolean filter(String arg0) throws Exception
            {
                // TODO Auto-generated method stub
                return arg0.equals("duan") || arg0.equals("zhixing");
            }
        });

        //一进多出算子
        SingleOutputStreamOperator<Tuple2<String, Integer>> flatMap = filter.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
        {

            @Override
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception
            {

                out.collect(new Tuple2<>(value, 1));
            }
        });

        //按照key分组算子
        KeyedStream<Tuple2<String, Integer>, Tuple> keyBy = flatMap.keyBy(0);

        //reduce，对上个数据结果进行计算
        SingleOutputStreamOperator<Tuple2<String, Integer>> reduce = keyBy.reduce(new ReduceFunction<Tuple2<String, Integer>>()
        {

            @Override
            public Tuple2<String, Integer> reduce(Tuple2<String, Integer> value1, Tuple2<String, Integer> value2) throws Exception
            {
                // TODO Auto-generated method stub
                return new Tuple2<>(value1.f0, value1.f1 + value2.f1);
            }
        });

        keyBy.print().setParallelism(1);
        env.execute();
    }
}
