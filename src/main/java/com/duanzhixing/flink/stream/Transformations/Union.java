/**
 * 
 */
package com.duanzhixing.flink.stream.Transformations;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import com.duanzhixing.flink.stream.source.MySourcePara;

/**
 * @author duanzhixing
 *将两个数据源合并,两个数据源数据类型相同
 */
public class Union
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> source1 = env.addSource(new MySourcePara());

        DataStreamSource<String> source2 = env.addSource(new SourceFunction<String>()
        {

            @Override
            public void run(org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext<String> ctx) throws Exception
            {
                for (int i = 0; i < 10; i++)
                {
                    Thread.currentThread().sleep(1000);

                    ctx.collect("hello");
                }

            }

            @Override
            public void cancel()
            {
                // TODO Auto-generated method stub

            }

        });

        DataStream<String> union = source1.union(source2);

        SingleOutputStreamOperator<String> map = union.map(new MapFunction<String, String>()
        {

            @Override
            public String map(String value) throws Exception
            {
                // TODO Auto-generated method stub
                return value;
            }
        });

        map.print().setParallelism(1);
        env.execute();
    }
}
