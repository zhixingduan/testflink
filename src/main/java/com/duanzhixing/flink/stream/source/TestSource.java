/**
 * 
 */
package com.duanzhixing.flink.stream.source;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author 调用自定义的数据源
 *这个是没有并行度的，加载有并行度的自定义数据源需要实现ParallelSourceFunction ，RichParallelSourceFunction 
 *
 *RichParallelSourceFunction 里有open和close方法，只执行一次，适合打开数据库连接，关闭数据库连接
 */
public class TestSource
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        MySourcePara mySourceNoPara = new MySourcePara();

        //
        DataStreamSource<String> addSource = env.addSource(mySourceNoPara);
        SingleOutputStreamOperator<String> map = addSource.map(new MapFunction<String, String>()
        {

            @Override
            public String map(String arg0) throws Exception
            {

                return arg0 + "7777";
            }
        });
        map.print().setParallelism(1);

        env.execute();
    }
}
