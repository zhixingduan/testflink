/**
 * 
 */
package com.duanzhixing.flink.stream.source;

import java.util.HashMap;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author duanzhixing
 *
 *测试redis source
 */
public class TestRedisSource
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<HashMap<String, String>> addSource = env.addSource(new MyRedisSource());

        addSource.print().setParallelism(1);
        env.execute("test redis");
    }
}
