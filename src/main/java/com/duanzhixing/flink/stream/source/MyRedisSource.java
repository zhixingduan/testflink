/**
 * 
 */
package com.duanzhixing.flink.stream.source;

import java.util.HashMap;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

import redis.clients.jedis.Jedis;

/**
 * @author duanzhixing
 *
 */
public class MyRedisSource extends RichSourceFunction<HashMap<String, String>>
{

    private Jedis jedis;

    //初始化资源链接，只执行一次
    @Override
    public void open(Configuration parameters) throws Exception
    {
        jedis = new Jedis("node1", 6379);
    }

    /**
     * 执行导入数据
     * */
    @Override
    public void run(org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext<HashMap<String, String>> ctx) throws Exception
    {
        String key = "k1";

        HashMap<String, String> map = new HashMap<>();
        map.put(key, jedis.get(key));
        ctx.collect(map);
    }

    /* 
     * 
     * 关闭资源链接
     */
    @Override
    public void cancel()
    {
        if (jedis != null)
        {
            jedis.close();

        }

    }

}
