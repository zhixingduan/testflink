/**
 * 
 */
package com.duanzhixing.flink.stream.source;

import java.util.ArrayList;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author duanzhixing
 *
 */
public class SourcefromCollection
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        ArrayList<String> data = new ArrayList<>();

        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        DataStreamSource<String> fromCollection = env.fromCollection(data);
        SingleOutputStreamOperator<String> map = fromCollection.map(new MapFunction<String, String>()
        {

            @Override
            public String map(String arg0) throws Exception
            {
                // TODO Auto-generated method stub
                return arg0 + "66666";
            }
        });
        map.print().setParallelism(1);
        env.execute();
    }
}
