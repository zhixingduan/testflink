/**
 * 
 */
package com.duanzhixing.flink.stream.source;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author Administrator
 *
 */
public class TestRedis
{

    public static void main(String[] args) throws InterruptedException
    {

        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(20);
        JedisPool pool = new JedisPool(jedisPoolConfig, "node1");
        ExecutorService javapool = Executors.newFixedThreadPool(20);
        String key1 = "k1";
        String key2 = "k2";

        String value = null;
        long start = System.currentTimeMillis();

        for (int i = 0; i < 300000; i++)
        {
            javapool.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    String value = null;

                    Jedis jedis = pool.getResource();

                    value = jedis.get(key1);

                    System.out.println(value);
                    jedis.close();
                }
            });
            System.out.println(i);
        }
        long end = System.currentTimeMillis();

        Thread.currentThread().join();
        System.out.println("执行完毕" + (end - start));
        javapool.shutdown();
        if (null != pool)
        {
            pool.destroy();
            System.out.println("连接池关闭");
        }
    }
}
