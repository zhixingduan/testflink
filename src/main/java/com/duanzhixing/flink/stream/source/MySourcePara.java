/**
 * 
 */
package com.duanzhixing.flink.stream.source;

import java.util.ArrayList;

import org.apache.flink.streaming.api.functions.source.SourceFunction;

/**
 * @author duanzhixing
 * 
 * 自定义流数据源
 *
 */
public class MySourcePara implements SourceFunction<String>
{

    /* 这里调用这个run方法，循环产生数据
     * 
     * */
    @Override
    public void run(SourceContext<String> ctx) throws Exception
    {

        //这段代码，使得 标记为当前source为空闲，暂时参与下游数据标记水印
        //                ctx.markAsTemporarilyIdle();

        ArrayList<String> data = new ArrayList<>();

        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");
        data.add("duan");
        data.add("zhixing");
        data.add("zuishuai");

        //循环产生数据
        for (String word : data)
        {
            Thread.currentThread().sleep(1000);
            ctx.collect(word);

        }

    }

    /* 
     * 当停止job任务的时候调用
     */
    @Override
    public void cancel()
    {
        // TODO Auto-generated method stub

    }

}
