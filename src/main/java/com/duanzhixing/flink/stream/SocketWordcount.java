/**
 * 
 */
package com.duanzhixing.flink.stream;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *统计每隔一秒计算最近两秒内单词个数计算
 */
public class SocketWordcount
{

    public static void main(String[] args) throws Exception
    {
        //1.获取运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //2.获取jar包入参，socket的ip和端口
        String hostname;
        int port;
        ParameterTool tool = ParameterTool.fromArgs(args);
        hostname = tool.get("hostname", "node2");
        port = tool.getInt("port", 9000);

        //指定数据分隔符
        String delimiter = "\n";
        //3.获取输入数据
        DataStreamSource<String> socketTextStream = env.socketTextStream(hostname, port, delimiter);

        //4.将一行数据拆分成单词 key-v数据
        SingleOutputStreamOperator<WordCount> flatMap = socketTextStream.flatMap(new FlatMapFunction<String, WordCount>()
        {

            @Override
            public void flatMap(String arg0, Collector<WordCount> coll) throws Exception
            {
                String[] split = arg0.split("\\s");
                for (String word : split)
                {
                    coll.collect(new WordCount(word, 1l));
                }

            }
        });

        //5.根据word属性进行分组
        KeyedStream<WordCount, Tuple> keyBy = flatMap.keyBy("word");

        //6.设置窗口数据，每隔一秒统计前两秒的数据
        WindowedStream<WordCount, Tuple, TimeWindow> timeWindow = keyBy.timeWindow(Time.seconds(2), Time.seconds(1));

        //7.相同的一组数据，按照count字段属性进行累加统计
        SingleOutputStreamOperator<WordCount> sum = timeWindow.sum("count");

        //8.设置一个并行度且打印
        sum.print().setParallelism(1);

        //9.提交到集群并打印
        env.execute();
    }

    public static class WordCount
    {

        public WordCount()
        {
        }

        /**
         * @param word
         * @param count
         */
        public WordCount(String word, long count)
        {
            this.word = word;
            this.count = count;
        }

        public String word;
        public long count;

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            return "WordCount [word=" + word + ", count=" + count + "]";
        }

    }
}
