/**
 * 
 */
package com.duanzhixing.flink.kafka;

import java.util.Properties;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

import com.duanzhixing.flink.stream.source.MySourcePara;

/**
 * @author duanzhixing
 *
 *将flink处理完的数据写入到kafka中
 */
public class MyKafkaSink
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //1s 拍一次快照
        env.enableCheckpointing(1000);

        DataStreamSource<String> addSource = env.addSource(new MySourcePara());

        Properties properties = new Properties();

        //设置kafka地址
        properties.setProperty("bootstrap.servers", "node1:9092");
        // only required for Kafka 0.8
        properties.setProperty("zookeeper.connect", "node2:2181,node3:2181,node4:2181");
        properties.setProperty("group.id", "test");

        FlinkKafkaProducer011<String> kafkaSink = new FlinkKafkaProducer011<String>("test", new SimpleStringSchema(), properties);

        //保存到快照
        //        kafkaSink.setFlushOnCheckpoint(false);
        //设置输出错误即停止
        kafkaSink.setLogFailuresOnly(true);
        addSource.addSink(kafkaSink).setParallelism(1);

        env.execute("testFlinkKafkaSink");
    }
}
