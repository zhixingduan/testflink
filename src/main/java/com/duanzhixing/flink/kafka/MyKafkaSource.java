/**
 * 
 */
package com.duanzhixing.flink.kafka;

import java.util.Properties;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;

/**
 * @author duanzhixing
 *
 *从kafka里面拿数据，然后进行wordcount的统计
 */
public class MyKafkaSource
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Properties properties = new Properties();

        //开启checkpoint ,保存offset 和operator 信息
        env.enableCheckpointing(1000);

        //设置kafka地址
        properties.setProperty("bootstrap.servers", "192.168.40.102:9092");
        // only required for Kafka 0.8
        properties.setProperty("zookeeper.connect", "192.168.40.102:2181,192.168.40.103:2181,192.168.40.104:2181");
        properties.setProperty("group.id", "test");
        //这里kafka接收到的数据是string 所以泛型写string,FlinkKafkaConsumer 需要跟kafka版本一致
        //        DataStreamSource<String> addSource = env.addSource(new FlinkKafkaConsumer<String>("topic1", new SimpleStringSchema(), properties));
        FlinkKafkaConsumer011<String> myKafka = new FlinkKafkaConsumer011<String>("test", new SimpleStringSchema(), properties);

        myKafka.setStartFromGroupOffsets();
        DataStreamSource<String> addSource = env.addSource(myKafka);

        SingleOutputStreamOperator<Tuple2<String, Integer>> reduce = addSource.map(new MapFunction<String, Tuple2<String, Integer>>()
        {

            @Override
            public Tuple2<String, Integer> map(String value) throws Exception
            {
                // TODO Auto-generated method stub
                return new Tuple2<>(value, 1);
            }
        }).keyBy(0).reduce(new ReduceFunction<Tuple2<String, Integer>>()
        {

            @Override
            public Tuple2<String, Integer> reduce(Tuple2<String, Integer> value1, Tuple2<String, Integer> value2) throws Exception
            {

                return new Tuple2<String, Integer>(value1.f0, value1.f1 + value2.f1);
            }
        });

        reduce.print().setParallelism(1);

        env.execute();
    }
}
