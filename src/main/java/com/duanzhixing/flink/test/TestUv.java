package com.duanzhixing.flink.test;

import java.util.HashSet;
import java.util.Set;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ContinuousProcessingTimeTrigger;

/**
 * 统计当前每天的用户  Uv，
 * @author duanzhixing
 * 读取socket 数据格式：a,userId,时间戳
 *                  
 */
public class TestUv
{
    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> source = env.socketTextStream("localhost", 7777);
        SingleOutputStreamOperator<Tuple2<String, String>> map = source.map(new MapFunction<String, Tuple2<String, String>>()
        {

            @Override
            public Tuple2<String, String> map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new Tuple2<String, String>(split[0], split[1]);
            }
        });

        SingleOutputStreamOperator<Long> aggregate = map.keyBy(0).windowAll(TumblingProcessingTimeWindows.of(Time.days(1), Time.hours(-8)))

                //1s触发一次计算
                .trigger(ContinuousProcessingTimeTrigger.of(Time.seconds(1)))
                //
                .aggregate(new AggregateFunction<Tuple2<String, String>, Set<String>, Long>()
                {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public Set<String> createAccumulator()
                    {
                        return new HashSet<String>();
                    }

                    @Override
                    public Set<String> add(Tuple2<String, String> value, Set<String> accumulator)
                    {

                        System.out.println("agg>>" + value.toString());
                        accumulator.add(value.f1);
                        if (value.f1.equals("user3"))
                        {
                            throw new RuntimeException("flink 抛异常出来啊  demo");
                        }
                        System.out.println("aggAccMap>>" + accumulator.toString());

                        return accumulator;
                    }

                    @Override
                    public Long getResult(Set<String> accumulator)
                    {
                        System.out.println("result>>" + accumulator.toString());

                        return Long.valueOf(accumulator.size());
                    }

                    @Override
                    public Set<String> merge(Set<String> a, Set<String> b)
                    {
                        boolean addAll = a.addAll(b);
                        return a;
                    }
                });

        aggregate.print("result>>").setParallelism(1);

        env.execute();
    }
}
