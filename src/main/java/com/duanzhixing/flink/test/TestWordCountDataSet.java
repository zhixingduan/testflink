package com.duanzhixing.flink.test;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.AggregateOperator;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.operators.MapOperator;
import org.apache.flink.api.java.operators.SortPartitionOperator;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

public class TestWordCountDataSet
{

    public static void main(String[] args) throws Exception
    {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        DataSource<String> readTextFile = env.readTextFile("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt");
        FlatMapOperator<String, String> flatMap = readTextFile.flatMap(new FlatMapFunction<String, String>()
        {

            @Override
            public void flatMap(String value, Collector<String> out) throws Exception
            {
                String[] split = value.split(" ");
                for (String word : split)
                {
                    out.collect(word);
                }
            }
        });

        MapOperator<String, Tuple2<String, Integer>> map = flatMap.map(new MapFunction<String, Tuple2<String, Integer>>()
        {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception
            {
                // TODO Auto-generated method stub
                return new Tuple2<String, Integer>(value, 1);
            }
        });
        UnsortedGrouping<Tuple2<String, Integer>> groupBy = map.groupBy(0);
        AggregateOperator<Tuple2<String, Integer>> sum = groupBy.sum(1);
        SortPartitionOperator<Tuple2<String, Integer>> sortPartition = sum.sortPartition(1, Order.DESCENDING);
        sortPartition.setParallelism(1);

        sortPartition.print();
    }
}
