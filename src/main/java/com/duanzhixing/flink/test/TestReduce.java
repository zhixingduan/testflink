package com.duanzhixing.flink.test;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class TestReduce
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> readTextFile = env.readTextFile("D:\\workspace\\testflink\\src\\main\\resources\\hello.txt");
        SingleOutputStreamOperator<SensorReading> map = readTextFile.map(new MapFunction<String, SensorReading>()
        {

            @Override
            public SensorReading map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SensorReading(split[0], Long.valueOf(split[1]), Double.valueOf(split[2]));
            }
        });

        KeyedStream<SensorReading, Tuple> keyBy = map.keyBy("id");
        SingleOutputStreamOperator<SensorReading> reduce = keyBy.reduce(new ReduceFunction<SensorReading>()
        {

            @Override
            public SensorReading reduce(SensorReading value1, SensorReading value2) throws Exception
            {
                if (value1.getTemperature() > value2.getTemperature())
                {
                    return value1;
                }
                return value2;
            }
        });

        reduce.print();
        env.execute();

    }
}
