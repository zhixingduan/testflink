package com.duanzhixing.flink.test;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.util.Collector;

public class TestSource
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> addSource = env.addSource(new SourceFunction<String>()
        {

            @Override
            public void run(SourceContext<String> ctx) throws Exception
            {
                for (int i = 0; i < 100; i++)
                {
                    ctx.collect("duan zhixing" + i);
                    Thread.currentThread().sleep(1000);

                }

            }

            @Override
            public void cancel()
            {
                // 关闭source的

            }

        });

        //map进行1对1的处理
        SingleOutputStreamOperator<String> map = addSource.map(new MapFunction<String, String>()
        {

            @Override
            public String map(String value) throws Exception
            {
                return value;
            }
        });

        //1对多打散
        SingleOutputStreamOperator<Tuple2<String, Integer>> flatMap = map.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
        {

            @Override
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception
            {
                String[] split = value.split(" ");
                for (String text : split)
                {
                    out.collect(new Tuple2<String, Integer>(text, 1));
                }

            }
        });

        //过滤
        SingleOutputStreamOperator<Tuple2<String, Integer>> filter = flatMap.filter(new FilterFunction<Tuple2<String, Integer>>()
        {

            @Override
            public boolean filter(Tuple2<String, Integer> value) throws Exception
            {
                String f0 = value.f0;
                if (f0.startsWith("duan"))
                {
                    return false;

                }
                return true;
            }
        });

        KeyedStream<Tuple2<String, Integer>, Tuple> keyBy = filter.keyBy(0);
        keyBy.sum(1).print("sum");
        keyBy.max(1).print("max");

        //最后一定要提交执行
        env.execute();
    }
}
