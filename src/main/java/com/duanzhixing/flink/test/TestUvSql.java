package com.duanzhixing.flink.test;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

/**
 * @author duanzhixing
 *输入数据socket id,userId ,time
 */
public class TestUvSql
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 4. 创建表执行环境，用blink版本
        EnvironmentSettings settings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
        StreamTableEnvironment streamEnv = StreamTableEnvironment.create(env, settings);

        DataStreamSource<String> source = env.socketTextStream("localhost", 7777);
        SingleOutputStreamOperator<SourceMod> map = source.map(new MapFunction<String, SourceMod>()
        {

            @Override
            public SourceMod map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SourceMod(split[0], split[1], split[2]);
            }
        });
        Table fromDataStream = streamEnv.fromDataStream(map, "id,userId,tstime,pt.proctime");
        streamEnv.createTemporaryView("inputTable", fromDataStream);

        //直接group by,日期得是2017-12-13 11:27:58这样的
        //        Table grouppv = streamEnv.sqlQuery("select DATE_FORMAT(tstime, 'yyyyMMdd') as riqi,count(*)  from inputTable group by DATE_FORMAT(tstime, 'yyyyMMdd')");
        //        streamEnv.toRetractStream(grouppv, Row.class).print("grouppv>>").setParallelism(1);

        Table groupuv = streamEnv.sqlQuery("select DATE_FORMAT(tstime, 'yyyyMMdd') as riqi,count(distinct userId)  from inputTable group by DATE_FORMAT(tstime, 'yyyyMMdd')");
        streamEnv.toRetractStream(groupuv, Row.class).print("groupuv>>").setParallelism(1);

        //over 窗口pv访问，当日系统的
        //        Table sqlQpv = streamEnv.sqlQuery("select count(*) over ( PARTITION BY DATE_FORMAT(pt, 'yyyyMMdd') ORDER BY pt  RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND CURRENT ROW) from inputTable");
        //        streamEnv.toRetractStream(sqlQpv, Row.class).print("uv>>").setParallelism(1);

        //over窗口 uv访问
        //        Table sqluv = streamEnv.sqlQuery("select userId,count(distinct userId) over ( PARTITION BY DATE_FORMAT(pt, 'yyyyMMdd') ORDER BY pt RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND CURRENT ROW) from inputTable");
        //        streamEnv.toRetractStream(sqluv, Row.class).print("result>>").setParallelism(1);
        env.execute();
    }

    public static class SourceMod
    {
        private String id;
        private String userId;
        private String tstime;

        public SourceMod()
        {
            super();
        }

        public SourceMod(String id, String userId, String tstime)
        {
            super();
            this.id = id;
            this.userId = userId;
            this.tstime = tstime;
        }

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getUserId()
        {
            return userId;
        }

        public void setUserId(String userId)
        {
            this.userId = userId;
        }

        public String getTstime()
        {
            return tstime;
        }

        public void setTstime(String tstime)
        {
            this.tstime = tstime;
        }

    }
}
