package com.duanzhixing.flink.test;

import org.apache.avro.generic.GenericData;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.omg.DynamicAny._DynArrayStub;

import java.util.ArrayList;
import java.util.List;

public class TestWCDateStream
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //        DataStreamSource<String> stream = env.readTextFile("D:\\eclipse-workspace\\testflink\\src\\main\\resources\\hello.txt");
        DataStreamSource<String> stream = env.socketTextStream("linux1", 7777);
        SingleOutputStreamOperator<Tuple2<String, Integer>> flatMap = stream.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
        {

            @Override
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception
            {
                String[] split = value.split(" ");
                for (String world : split)
                {
                    out.collect(new Tuple2<String, Integer>(world, 1));
                }

            }
        });

       List<String> list =new ArrayList<>();
        boolean a = list.add("a");

        SingleOutputStreamOperator<Tuple2<String, Integer>> sum = flatMap.keyBy(0).sum(1);
        sum.setParallelism(1).print();
        env.execute();
    }


}
