package com.duanzhixing.flink.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;

public class SensorReading
{

    private String id;

    private Long time;

    private Double temperature;

    public SensorReading()
    {
        super();
    }

    public SensorReading(String id, Long time, Double temperature)
    {
        super();
        this.id = id;
        this.time = time;
        this.temperature = temperature;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getTime()
    {
        return time;
    }

    public void setTime(Long time)
    {
        this.time = time;
    }

    public Double getTemperature()
    {
        return temperature;
    }

    public void setTemperature(Double temperature)
    {
        this.temperature = temperature;
    }

    @Override
    public String toString()
    {
        return "SensorReading [id=" + id + ", time=" + time + ", temperature=" + temperature + "]";
    }

    public static void main(String[] args)
    {
        List<SensorReading> list = new ArrayList<SensorReading>();
        for (int i = 0; i < 10; i++)
        {
            SensorReading s = new SensorReading();
            s.setId("sensro_" + i);
            s.setTemperature(36.0 + i);
            s.setTime(new Date().getTime());
            System.out.println(JSON.toJSON(s));
        }
    }

}
