/**
 * 
 */
package com.duanzhixing.flink.batch.transformations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.MapPartitionOperator;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *mapPartition 算子与map功能相同，但是它获取的是一次获取一个分区的数据，适合在map中要打开数据连接等操作
 */
public class MapPartition
{

    public static void main(String[] args) throws Exception
    {
        //1.获取运行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        String filePath = "E:\\data\\file";
        //2.读取要计算的文件
        DataSource<String> readTextFile = env.readTextFile(filePath);

        MapPartitionOperator<String, String> mapPartition = readTextFile.mapPartition(new MapPartitionFunction<String, String>()
        {

            @Override
            public void mapPartition(Iterable<String> values, Collector<String> out) throws Exception
            {

                Connection connection = null;
                Statement statement = null;
                ResultSet resultSet = null;

                Class.forName("com.mysql.jdbc.Driver");
                System.out.println("成功加载驱动");
                String url = "jdbc:mysql://node1:3306/test?user=root&password=123&useUnicode=true&characterEncoding=UTF8";
                connection = DriverManager.getConnection(url);
                System.out.println("成功获取连接");

                PreparedStatement pstmt;

                Iterator<String> iterator = values.iterator();
                while (iterator.hasNext())
                {

                    String[] split = iterator.next().split("\\s");

                    for (String word : split)
                    {
                        String sql = "insert into person (name) values('" + word + "')";
                        pstmt = connection.prepareStatement(sql);
                        pstmt.executeUpdate(sql);//执行sql语句
                        System.out.println("成功操作数据库");
                    }

                }

                if (resultSet != null)
                {
                    resultSet.close();
                }
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }

            }
        }).setParallelism(3);

        mapPartition.print();
        //        env.execute();
    }
}
