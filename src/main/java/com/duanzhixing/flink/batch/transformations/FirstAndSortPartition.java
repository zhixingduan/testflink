/**
 * 
 */
package com.duanzhixing.flink.batch.transformations;

import java.util.List;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.AggregateOperator;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *Fist-n 就是取数据的前几个
 *
 *Sort Partition 在本地对数据集的所有分区进行排序，通过sortPartition()的链接调用来完成对多个字段的排序
 *
 *我们下面根据word count为例子演示使用
 */
public class FirstAndSortPartition
{

    public static void main(String[] args) throws Exception
    {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        //这里面存放的是一堆单词
        String filePath = "E:\\data\\file";

        DataSource<String> readTextFile = env.readTextFile(filePath);
        UnsortedGrouping<Tuple2<String, Integer>> groupBy = readTextFile.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
        {

            private static final long serialVersionUID = 1L;

            @Override
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception
            {
                String[] split = value.split("\\s");
                for (String string : split)
                {
                    out.collect(new Tuple2<>(string, 1));
                }

            }
        }).groupBy(0);

        AggregateOperator<Tuple2<String, Integer>> sum = groupBy.sum(1);

        //        sum.print();

        sum.sortPartition(1, Order.DESCENDING).print();
        //        List<Tuple2<String, Integer>> collect = sum.collect();

        System.out.println("========================================");

        List<Tuple2<String, Integer>> collect = sum.collect();
        DataSource<Tuple2<String, Integer>> fromCollection = env.fromCollection(collect);
        fromCollection.sortPartition(1, Order.DESCENDING).print();
        //分组排序
        //        groupBy.sortGroup(1, Order.DESCENDING).first(2).print();

        //获取运行环境
        //        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        //
        //        ArrayList<Tuple2<Integer, String>> data = new ArrayList<>();
        //        data.add(new Tuple2<>(2, "zs"));
        //        data.add(new Tuple2<>(4, "ls"));
        //        data.add(new Tuple2<>(3, "ww"));
        //        data.add(new Tuple2<>(1, "xw"));
        //        data.add(new Tuple2<>(1, "aw"));
        //        data.add(new Tuple2<>(1, "mw"));
        //        data.add(new Tuple2<>(8, "mw"));
        //        data.add(new Tuple2<>(4, "mw"));
        //
        //        DataSource<Tuple2<Integer, String>> text = env.fromCollection(data);
        //
        //        //获取前3条数据，按照数据插入的顺序
        //        text.print();
        //        System.out.println("==============================");
        //
        //        //根据数据中的第二列进行分组，获取每组的前2个元素
        //        text.groupBy(1).first(2).print();
        //        System.out.println("==============================");
        //
        //        //根据数据中的第二列分组，再根据第一列进行组内排序[升序]，获取每组的前2个元素
        //        text.groupBy(1).sortGroup(0, Order.ASCENDING).first(2).print();
        //        System.out.println("==============================");
        //
        //        //不分组，全局排序获取集合中的前3个元素，针对第一个元素升序，第二个元素倒序
        //        text.sortPartition(0, Order.DESCENDING).sortPartition(1, Order.DESCENDING).first(3).print();
    }
}
