/**
 * 
 */
package com.duanzhixing.flink.batch.transformations;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.DistinctOperator;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *Distinct 算子对字段进行去重
 *
 */
public class Distinct
{
    public static void main(String[] args) throws Exception
    {
        //1.获取运行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        String filePath = "E:\\data\\file";
        //2.读取要计算的文件
        DataSource<String> readTextFile = env.readTextFile(filePath);

        FlatMapOperator<String, Tuple2<String, Integer>> flatMap = readTextFile.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
        {

            @Override
            public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception
            {
                String[] split = value.split("\\s");
                for (String word : split)
                {
                    out.collect(new Tuple2<>(word, 1));
                }
            }
        });

        DistinctOperator<Tuple2<String, Integer>> distinct = flatMap.distinct(0);

        distinct.print();
        //        env.execute();
    }

}
