/**
 * 
 */
package com.duanzhixing.flink.batch.transformations;

import java.util.ArrayList;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

/**
 * @author duanzhixing
 * 
 * Join为内连接
 * OuterJoin为外链接
 *
 */
public class Join
{
    public static void main(String[] args) throws Exception
    {

        //获取运行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        //tuple2<用户id，用户姓名>
        ArrayList<Tuple2<Integer, String>> data1 = new ArrayList<>();
        data1.add(new Tuple2<>(1, "zs"));
        data1.add(new Tuple2<>(2, "ls"));
        data1.add(new Tuple2<>(3, "ww"));

        //tuple2<用户id，用户所在城市>
        ArrayList<Tuple2<Integer, String>> data2 = new ArrayList<>();
        data2.add(new Tuple2<>(1, "beijing"));
        data2.add(new Tuple2<>(2, "shanghai"));
        data2.add(new Tuple2<>(4, "guangzhou"));

        DataSource<Tuple2<Integer, String>> text1 = env.fromCollection(data1);
        DataSource<Tuple2<Integer, String>> text2 = env.fromCollection(data2);

        //内连接
        text1.join(text2).where(0)//指定第一个数据集中需要进行比较的元素角标
                .equalTo(0)//指定第二个数据集中需要进行比较的元素角标
                .with(new JoinFunction<Tuple2<Integer, String>, Tuple2<Integer, String>, Tuple3<Integer, String, String>>()
                {
                    @Override
                    public Tuple3<Integer, String, String> join(Tuple2<Integer, String> first, Tuple2<Integer, String> second) throws Exception
                    {
                        return new Tuple3<>(first.f0, first.f1, second.f1);
                    }
                }).print();

        System.out.println("==================================");

        //左外链接
        text1.leftOuterJoin(text2).where(0).equalTo(0).with(new JoinFunction<Tuple2<Integer, String>, Tuple2<Integer, String>, Tuple3<Integer, String, String>>()
        {

            @Override
            public Tuple3<Integer, String, String> join(Tuple2<Integer, String> first, Tuple2<Integer, String> second) throws Exception
            {

                if (second == null)
                {
                    return new Tuple3<>(first.f0, first.f1, "");

                }
                return new Tuple3<>(first.f0, first.f1, second.f1);
            }
        }).print();

        System.out.println("==================================");

        //右外链接
        text1.rightOuterJoin(text2).where(0).equalTo(0).with(new JoinFunction<Tuple2<Integer, String>, Tuple2<Integer, String>, Tuple3<Integer, String, String>>()
        {

            @Override
            public Tuple3<Integer, String, String> join(Tuple2<Integer, String> first, Tuple2<Integer, String> second) throws Exception
            {

                if (first == null)
                {
                    return new Tuple3<>(second.f0, "", second.f1);

                }
                return new Tuple3<>(first.f0, first.f1, second.f1);
            }
        }).print();

        System.out.println("==================================");

        text1.fullOuterJoin(text2).where(0).equalTo(0).with(new JoinFunction<Tuple2<Integer, String>, Tuple2<Integer, String>, Tuple3<Integer, String, String>>()
        {

            @Override
            public Tuple3<Integer, String, String> join(Tuple2<Integer, String> first, Tuple2<Integer, String> second) throws Exception
            {

                if (first == null)
                {
                    return new Tuple3<>(second.f0, "", second.f1);

                }

                if (second == null)
                {
                    return new Tuple3<>(first.f0, first.f1, "");
                }
                return new Tuple3<>(first.f0, first.f1, second.f1);
            }
        }).print();
    }

}
