/**
 * 
 */
package com.duanzhixing.flink.batch;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.AggregateOperator;
import org.apache.flink.api.java.operators.DataSink;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *将一个文本文件数据，进行word count统计
 */
public class BatchWordCount
{

    public static void main(String[] args) throws Exception
    {

        //1.获取运行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        String filePath = "E:\\data\\file";
        //2.读取要计算的文件
        DataSource<String> readTextFile = env.readTextFile(filePath);

        //3.读取数据文件转换为k-v数据
        FlatMapOperator<String, Tuple2<String, Integer>> flatMap = readTextFile.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
        {

            @Override
            public void flatMap(String arg0, Collector<Tuple2<String, Integer>> arg1) throws Exception
            {
                String[] strings = arg0.split("\\s");
                for (String word : strings)
                {
                    arg1.collect(new Tuple2<>(word, 1));
                }

            }
        });

        //4.强k-v数据按照Tuple2的第一个字段（word）进行分组
        UnsortedGrouping<Tuple2<String, Integer>> groupBy = flatMap.groupBy(0);

        //5。按照Tuple2第二个字段进行sum处理
        AggregateOperator<Tuple2<String, Integer>> sum = groupBy.sum(1);

        //6.将结果数据并设置一个并行度
        DataSink<Tuple2<String, Integer>> setParallelism = sum.writeAsCsv("E:\\Data\\wordCount").setParallelism(1);

        //7.提交jar包并执行
        env.execute("word count by file");
    }
}
