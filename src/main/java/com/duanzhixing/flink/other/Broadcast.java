/**
 * 
 */
package com.duanzhixing.flink.other;

import java.util.List;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichFilterFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FilterOperator;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *广播变量
 *
 *场景：将关键数据广播出去，在filter端就会根据这份数据进行过滤
 *
 *需要实现RichFilterFunction 接口
 *需要使用open 方法
 */
public class Broadcast
{
    public static void main(String[] args) throws Exception
    {
        //1.获取运行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        String filePath = "E:\\data\\file";
        //2.读取要计算的文件
        DataSource<String> readTextFile = env.readTextFile(filePath);
        DataSet<String> toBroadcast = env.fromElements("hadoop", "zhixing");

        FlatMapOperator<String, String> flatMap = readTextFile.flatMap(new FlatMapFunction<String, String>()
        {

            @Override
            public void flatMap(String value, Collector<String> out) throws Exception
            {
                String[] split = value.split("\\s");
                for (String string : split)
                {
                    out.collect(string);
                }

            }
        });

        //        flatMap.print();

        System.out.println("============================");
        //这里需要使用richfilterfunction的接口，因为它继承了AbstractRichFunction，AbstractRichFunction 才有getRuntimeContext() 方法
        FilterOperator<String> withBroadcastSet = flatMap.filter(new RichFilterFunction<String>()
        {
            List<String> broadcastVariable = null;

            //这个方法一个节点上只会执行一次，获取全局广播变量
            @Override
            public void open(Configuration parameters) throws Exception
            {
                broadcastVariable = getRuntimeContext().getBroadcastVariable("broadcastSetName");

            }

            @Override
            public boolean filter(String value) throws Exception
            {

                if (broadcastVariable.contains(value))
                {
                    return true;
                }
                return false;
            }
        }).withBroadcastSet(toBroadcast, "broadcastSetName");//这里将广播变量广播出来

        withBroadcastSet.print();

    }
}
