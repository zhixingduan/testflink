/**
 * 
 */
package com.duanzhixing.flink.other;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;

/**
 * @author duanzhixing
 *
 */
public class Test
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> socketTextStream = env.socketTextStream("node2", 9000);

        SingleOutputStreamOperator<Tuple2<Integer, Integer>> map = socketTextStream.map(new MapFunction<String, Tuple2<Integer, Integer>>()
        {

            @Override
            public Tuple2<Integer, Integer> map(String value) throws Exception
            {
                return new Tuple2<>(1, Integer.valueOf(value));
            }
        });

        SingleOutputStreamOperator<Tuple2<Integer, Integer>> reduce = map.keyBy(0).timeWindow(Time.seconds(8)).reduce(new ReduceFunction<Tuple2<Integer, Integer>>()
        {

            @Override
            public Tuple2<Integer, Integer> reduce(Tuple2<Integer, Integer> value1, Tuple2<Integer, Integer> value2) throws Exception
            {
                // TODO Auto-generated method stub
                return new Tuple2<>(value1.f0, value1.f1 + value2.f1);
            }
        });

        reduce.print();

        SingleOutputStreamOperator<String> map2 = reduce.map(new MapFunction<Tuple2<Integer, Integer>, String>()
        {

            @Override
            public String map(Tuple2<Integer, Integer> value) throws Exception
            {
                System.out.println("执行map处理");
                return "执行map处理";
            }
        });

        map2.print();

        env.execute();

    }
}
