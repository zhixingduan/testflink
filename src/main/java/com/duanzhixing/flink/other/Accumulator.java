/**
 * 
 */
package com.duanzhixing.flink.other;

import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.common.accumulators.IntCounter;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.MapOperator;
import org.apache.flink.configuration.Configuration;

/**
 * @author duanzhixing
 *
 *累加器
 *
 *l用法
•1：创建累加器
•private IntCounter numLines = new IntCounter();
•2：注册累加器
•getRuntimeContext().addAccumulator("num-lines", this.numLines);
•3：使用累加器
•this.numLines.add(1);
•4：获取累加器的结果
•myJobExecutionResult.getAccumulatorResult("num-lines")


*注意：需要实现Ric的方法
*使用open方法获取注册累加器
 */
public class Accumulator
{

    public static void main(String[] args) throws Exception
    {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        DataSource<String> fromElements = env.fromElements("duan", "zhixing", "hadoop", "flink");

        //创建累加器
        IntCounter accumulator = new IntCounter();
        MapOperator<String, String> map = fromElements.map(new RichMapFunction<String, String>()
        {

            //这个方法只会执行一次
            @Override
            public void open(Configuration parameters) throws Exception
            {
                //注册累加器
                getRuntimeContext().addAccumulator("numCount", accumulator);
            }

            @Override
            public String map(String value) throws Exception
            {
                //使用累加器
                accumulator.add(1);
                return value;
            }
        }).setParallelism(3);

        map.writeAsText("E:\\data\\accumulatorResult.txt");

        //获取累加器的结果值
        JobExecutionResult execute = env.execute();
        int accumulatorResult = execute.getAccumulatorResult("numCount");
        System.out.println(accumulatorResult);
    }
}
