/**
 * 
 */
package com.duanzhixing.flink.other;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.common.functions.RichFilterFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FilterOperator;
import org.apache.flink.configuration.Configuration;

/**
 * @author duanzhixing
 * 
 * 场景：利用一个hdfs里的文件去过滤一些数据，如果这些数据在那个文件中则通过
 *
 *分布式文件缓存
 *
1：注册一个文件
env.registerCachedFile("hdfs:///path/to/your/file", "hdfsFile")  
2：访问数据
File myFile = getRuntimeContext().getDistributedCache().getFile("hdfsFile");

 */
public class DistributedCache
{

    public static void main(String[] args) throws Exception
    {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        //将hdfs上一个文件注册为缓存文件
        env.registerCachedFile("hdfs://node1:8020/flink/disCache.txt", "disCacheFile");

        DataSource<String> fromElements = env.fromElements("duan", "zhixing", "duan", "duan", "zhixing");

        FilterOperator<String> filter = fromElements.filter(new RichFilterFunction<String>()
        {

            private List<String> words = new ArrayList<>();

            //该方法只执行一次
            @Override
            public void open(Configuration parameters) throws Exception
            {
                //获取注册缓存的文件
                File file = getRuntimeContext().getDistributedCache().getFile("disCacheFile");

                List<String> readLines = org.apache.commons.io.FileUtils.readLines(file);

                for (String line : readLines)
                {
                    String[] split = line.split("\\s");
                    for (String word : split)
                    {
                        words.add(word);
                    }
                }

            }

            @Override
            public boolean filter(String value) throws Exception
            {
                System.out.println("有走到这里吗");
                if (words.contains(value))
                {
                    return true;
                }
                return false;
            }
        }).setParallelism(3);

        filter.print();

        //获取运行环境
        //        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        //
        //        //1：注册一个文件,可以使用hdfs或者s3上的文件
        //        env.registerCachedFile("hdfs://node1:8020/flink/disCache.txt", "a.txt");
        //
        //        DataSource<String> data = env.fromElements("a", "b", "c", "d");
        //
        //        DataSet<String> result = data.map(new RichMapFunction<String, String>()
        //        {
        //            private ArrayList<String> dataList = new ArrayList<String>();
        //
        //            @Override
        //            public void open(Configuration parameters) throws Exception
        //            {
        //                super.open(parameters);
        //                //2：使用文件
        //                File myFile = getRuntimeContext().getDistributedCache().getFile("a.txt");
        //                List<String> lines = FileUtils.readLines(myFile);
        //                for (String line : lines)
        //                {
        //                    this.dataList.add(line);
        //                    System.out.println("line:" + line);
        //                }
        //            }
        //
        //            @Override
        //            public String map(String value) throws Exception
        //            {
        //                //在这里就可以使用dataList
        //                return value;
        //            }
        //        });
        //
        //        result.print();
    }
}
