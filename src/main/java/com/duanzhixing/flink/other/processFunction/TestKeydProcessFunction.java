package com.duanzhixing.flink.other.processFunction;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import com.duanzhixing.flink.test.SensorReading;

/**
 * @author duanzhixing
 *
 */
public class TestKeydProcessFunction
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> socketTextStream = env.socketTextStream("localhost", 7777);
        SingleOutputStreamOperator<SensorReading> map = socketTextStream.map(new MapFunction<String, SensorReading>()
        {

            @Override
            public SensorReading map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SensorReading(split[0], Long.valueOf(split[1]), Double.valueOf(split[2]));
            }
        });

        map.keyBy("id").process(new MykeydProcessFunction()).print();
        env.execute();
    }

    public static class MykeydProcessFunction extends KeyedProcessFunction<Tuple, SensorReading, String>
    {

        private static final long serialVersionUID = 1L;

        //记录上一个数据流的温度值
        private ValueState<Double> lastTemperature;

        //记录上一次注册定时器时候的时间戳，标记是否有存在的定时器
        private ValueState<Long> lastTime;

        @Override
        public void open(Configuration parameters) throws Exception
        {
            //注册获取当前key存储的两个状态数据
            lastTemperature = getRuntimeContext().getState(new ValueStateDescriptor<Double>("my-lastTemperature", Double.class, 0.0));
            lastTime = getRuntimeContext().getState(new ValueStateDescriptor<Long>("my-lastTime", Long.class));

        }

        @Override
        public void processElement(SensorReading value, KeyedProcessFunction<Tuple, SensorReading, String>.Context ctx, Collector<String> out) throws Exception
        {
            Long lastTimeVlaue = lastTime.value();

            if (value.getTemperature() > lastTemperature.value() && lastTimeVlaue == null)
            {
                //如果当前温度比上次温度高且定时器为空，则注册一个10s的定时器，并标记注册了一个定时器

                //1、获取flink系统时间,并标记一个10s后的时间戳
                long currentProcessingTime = ctx.timerService().currentProcessingTime() + 10 * 1000;
                //2、注册一个10s后的定时器
                ctx.timerService().registerProcessingTimeTimer(currentProcessingTime);
                //3、标记定时器时间戳
                lastTime.update(currentProcessingTime);

            }
            else if (value.getTemperature() < lastTemperature.value() && lastTimeVlaue != null)
            {
                //如果当前温度比上次温度低且定时器不为空，则销毁定时器，清空标记状态
                ctx.timerService().deleteEventTimeTimer(lastTime.value());
                lastTime.clear();
            }

            lastTemperature.update(value.getTemperature());
        }

        @Override
        public void onTimer(long timestamp, KeyedProcessFunction<Tuple, SensorReading, String>.OnTimerContext ctx, Collector<String> out) throws Exception
        {
            //当触发定时器时候，输出预警报告
            out.collect("当前" + ctx.getCurrentKey().getField(0) + "发生了10s内温度连续上升的预警");
            lastTime.clear();
            System.out.println("当前" + ctx.getCurrentKey().getField(0) + "发生了10s内温度连续上升的预警");

        }

        @Override
        public void close() throws Exception
        {
            lastTemperature.clear();
            lastTime.clear();
        }
    }
}
