package com.duanzhixing.flink.other.state;

import java.util.Collections;
import java.util.List;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.checkpoint.ListCheckpointed;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author duanzhixing
 *
 *测试算子状态
 */
public class TestOperatorState
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        DataStreamSource<String> socketTextStream = env.socketTextStream("localhost", 7777);
        SingleOutputStreamOperator<Integer> map = socketTextStream.map(new MyMapFunction());
        map.print();
        env.execute();
    }

    public static class MyMapFunction implements MapFunction<String, Integer>, ListCheckpointed<Integer>
    {

        //我们设置一个state 用来保存当前分区的数据条数。我们会将count数据存储到ListCheckpointed 中
        Integer count = 0;

        //保存我们设置sate的方法
        @Override
        public List<Integer> snapshotState(long checkpointId, long timestamp) throws Exception
        {
            return Collections.singletonList(count);
        }

        //恢复我们存储的state的方法
        @Override
        public void restoreState(List<Integer> state) throws Exception
        {
            for (Integer num : state)
            {
                count += num;
            }

        }

        //map的方法
        @Override
        public Integer map(String value) throws Exception
        {
            count++;
            return count;
        }

    }

    //    public static class MyStateFunction extends RichMapFunction<String, String>{
    //
    //        private  MapState<String, String> mapState;
    //        
    //        @Override
    //        public String map(String value) throws Exception
    //        {
    //            return null;
    //        }
    //        
    //        @Override
    //        public void open(Configuration parameters) throws Exception
    //        {
    //            mapState = getRuntimeContext().getMapState(new MapStateDescriptor<String, String>("name", String.class, String.class));
    //        }
    //    }
}
