package com.duanzhixing.flink.other.state;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig.ExternalizedCheckpointCleanup;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @author duanzhixing
 *
 *数据快照，flink高容错机制是根据checkpoint来实现的
 *
 *场景：我们这里以socket 的word count来演示
 */
public class CheckPoint
{

    public static void main(String[] args) throws Exception
    {
        //1.获取运行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //        默认checkpoint功能是disabled的，想要使用的时候需要先启用
        // 每隔1000 ms进行启动一个检查点【设置checkpoint的周期】
        env.enableCheckpointing(1000);
        // 高级选项：
        // 设置模式为exactly-once （这是默认值）
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
        // 确保检查点之间有至少500 ms的间隔【checkpoint最小间隔】
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        // 检查点必须在一分钟内完成，或者被丢弃【checkpoint的超时时间】
        env.getCheckpointConfig().setCheckpointTimeout(60000);
        // 同一时间只允许进行一个检查点
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        // 表示一旦Flink处理程序被cancel后，会保留Checkpoint数据，以便根据实际需要恢复到指定的Checkpoint
        env.getCheckpointConfig().enableExternalizedCheckpoints(ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);

        //设置checkponint 快照存储位置
        env.setStateBackend(new FsStateBackend("file:///D:/eclipse-workspace/testflink/src/main/resources/stateBackend", true));

        //2.获取jar包入参，socket的ip和端口
        String hostname;
        int port;
        ParameterTool tool = ParameterTool.fromArgs(args);
        hostname = tool.get("hostname", "localhost");
        port = tool.getInt("port", 7777);

        //指定数据分隔符
        String delimiter = "\n";
        //3.获取输入数据
        DataStreamSource<String> socketTextStream = env.socketTextStream(hostname, port, delimiter);

        //4.将一行数据拆分成单词 key-v数据
        SingleOutputStreamOperator<WordCount> flatMap = socketTextStream.flatMap(new FlatMapFunction<String, WordCount>()
        {

            @Override
            public void flatMap(String arg0, Collector<WordCount> coll) throws Exception
            {
                String[] split = arg0.split("\\s");
                for (String word : split)
                {
                    coll.collect(new WordCount(word, 1l));
                }

            }
        });

        //5.根据word属性进行分组
        KeyedStream<WordCount, Tuple> keyBy = flatMap.keyBy("word");

        //7.相同的一组数据，按照count字段属性进行累加统计
        SingleOutputStreamOperator<WordCount> sum = keyBy.sum("count");

        //8.设置一个并行度且打印
        sum.print().setParallelism(1);

        //9.提交到集群并打印
        env.execute();
    }

    public static class WordCount
    {

        public WordCount()
        {
        }

        /**
         * @param word
         * @param count
         */
        public WordCount(String word, long count)
        {
            this.word = word;
            this.count = count;
        }

        public String word;
        public long count;

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            return "WordCount [word=" + word + ", count=" + count + "]";
        }

    }
}
