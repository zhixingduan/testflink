package com.duanzhixing.flink.other.state;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ReducingState;
import org.apache.flink.api.common.state.ReducingStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import com.duanzhixing.flink.test.SensorReading;

/**
 * @author duanzhixing
 *测试分组状态state,我们来统计每个不同id下数据的个数
 */
public class TestKeyState
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStateBackend(new FsStateBackend("hdfs://namenode:40010//file"));
        DataStreamSource<String> socketTextStream = env.socketTextStream("localhost", 7777);
        SingleOutputStreamOperator<SensorReading> map = socketTextStream.map(new MapFunction<String, SensorReading>()
        {
            @Override
            public SensorReading map(String value) throws Exception
            {
                String[] split = value.split(",");
                return new SensorReading(new String(split[0].getBytes(), "utf-8"), Long.valueOf(split[1]), Double.valueOf(split[2]));
            }
        });

        SingleOutputStreamOperator<Integer> map2 = map.keyBy("id").map(new RichMapFunction<SensorReading, Integer>()
        {
            //统计当前key下数据个数
            private ValueState<Integer> count;

            //测试一个reducing 的state，存储一个最大温度值
            private ReducingState<Double> reduce;

            @Override
            public void open(Configuration parameters) throws Exception
            {
                //从当前分区的上下文环境中拿出当前分区的key的状态state
                count = getRuntimeContext().getState(new ValueStateDescriptor<Integer>("my-count-state", Integer.class));

                //测试一个reducing 的state,就是将一个方法当成一个参数存储起来,存储计算温度最大的一个
                reduce = getRuntimeContext().getReducingState(new ReducingStateDescriptor("my-reducing-state", new MyReducingFunctiuong(), MyReducingFunctiuong.class));

            }

            @Override
            public Integer map(SensorReading value) throws Exception
            {
                Integer value2 = count.value();
                if (value2 == null)
                {
                    value2 = 0;
                }
                value2++;
                System.out.println(value.getId() + "count" + value2);
                count.update(value2);

                //测试测试一个reducing  state
                reduce.add(value.getTemperature());
                System.out.println(value.getId() + "max reducing" + reduce.get());
                return value2;
            }

        });

        env.execute();
    }

    public static class MyReducingFunctiuong implements ReduceFunction<Double>
    {
        //选取温度最大的一个值
        @Override
        public Double reduce(Double value1, Double value2) throws Exception
        {
            return Math.max(value1, value2);
        }

    }
}
