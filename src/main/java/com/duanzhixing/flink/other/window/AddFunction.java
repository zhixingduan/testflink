/**
 * 
 */
package com.duanzhixing.flink.other.window;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.windowing.time.Time;

/**
 * @author 增量聚合
 *
 *增量聚合的特点是，在该窗口时间内，来一条数据reduce一条，一般为ReduceFunction, AggregateFunction。这两种函数，如下是word count算法
 */
public class AddFunction
{
    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        SingleOutputStreamOperator<Tuple2<String, Integer>> map = env.addSource(new Mysource()).map(new MapFunction<String, Tuple2<String, Integer>>()
        {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception
            {
                return new Tuple2<>(value, 1);
            }
        });

        map.keyBy(0).timeWindow(Time.seconds(3)).reduce(new ReduceFunction<Tuple2<String, Integer>>()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Integer> reduce(Tuple2<String, Integer> value1, Tuple2<String, Integer> value2) throws Exception
            {

                return new Tuple2<>(value1.f0, value1.f1 + value2.f1);
            }
        }).print("reduce");

        //        AggregateFunction 能够获取到更多的相关参数
        //        Tuple2<String, Integer>, 第一个参数指的是入参类型
        //        Integer, 中间参数，计算过程中中间缓存类型，现在是count操作，，因此是累加器Integer类型
        //        Integer 最后参数指的是数据结果类型
        map.keyBy(0).timeWindowAll(Time.seconds(3)).aggregate(new AggregateFunction<Tuple2<String, Integer>, Integer, Integer>()
        {

            //初始化累加器
            @Override
            public Integer createAccumulator()
            {
                return 0;
            }

            //累加的数据运算
            @Override
            public Integer add(Tuple2<String, Integer> value, Integer accumulator)
            {
                return accumulator + value.f1;
            }

            //返回数据运算的结果
            @Override
            public Integer getResult(Integer accumulator)
            {
                return accumulator;
            }

            //暂时不用理会
            @Override
            public Integer merge(Integer a, Integer b)
            {
                // TODO Auto-generated method stub
                return null;
            }

        }).print("AggregateFunction");

        env.execute();

    }

    protected static class Mysource implements SourceFunction<String>
    {

        @Override
        public void run(SourceContext<String> ctx) throws Exception
        {
            for (int i = 0; i < 20; i++)
            {

                ctx.collect("duanzhixing" + i);

                ctx.collect("duanzhixing" + i);
                Thread.currentThread().sleep(1000);

            }

        }

        @Override
        public void cancel()
        {
            // TODO Auto-generated method stub

        }

    }

}
