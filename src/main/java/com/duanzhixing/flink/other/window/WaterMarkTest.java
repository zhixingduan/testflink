package com.duanzhixing.flink.other.window;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import com.duanzhixing.flink.test.SensorReading;

public class WaterMarkTest
{

    public static void main(String[] args) throws Exception
    {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //为方便测试，设置系统并行度为1
        env.setParallelism(1);

        //指定flink系统使用数据的事件时间作为系统计算单位时间
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        DataStreamSource<String> socketTextStream = env.socketTextStream("localhost", Integer.valueOf("7777"));

        SingleOutputStreamOperator<SensorReading> flatMap = socketTextStream.flatMap(new FlatMapFunction<String, SensorReading>()
        {
            @Override
            public void flatMap(String value, Collector<SensorReading> out) throws Exception
            {
                System.out.println(value);
                String[] split = value.split(",");
                out.collect(new SensorReading(split[0], Long.valueOf(split[1]), Double.valueOf(split[2])));
            }
        });

        //我们设置一个watermark时间提取器，这里设置BoundedOutOfOrdernessTimestampExtractor，有界无序时间提取器。一般情况下watermark 是毫秒级别的，这里为了方便测试设置为2s
        SingleOutputStreamOperator<SensorReading> assignTimestampsAndWatermarks = flatMap.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2))
        {
            @Override
            public long extractTimestamp(SensorReading element)
            {
                //为了方便测试，我们输入socket的时间是秒级别的，所以这里需要转化为毫秒
                return element.getTime() * 1000;
            }
        });

        //对socket数据进行开窗,
        WindowedStream<SensorReading, Tuple, TimeWindow> timeWindow = assignTimestampsAndWatermarks.keyBy("id").timeWindow(Time.seconds(5));

        //设置延迟关闭窗口时间1分钟，该时间内窗口不关闭，且迟到的数据仍然能参与计算
        timeWindow.allowedLateness(Time.minutes(1));

        //设置关闭窗口后的数据，进入到test late流中，最后test late 做输出查看
        timeWindow.sideOutputLateData(new OutputTag<SensorReading>("test late")
        {
        });

        //执行计算
        SingleOutputStreamOperator<SensorReading> minBy = timeWindow.minBy("temperature");
        minBy.print("minby");

        //最后回收test late 流
        DataStream<SensorReading> sideOutput = minBy.getSideOutput(new OutputTag<SensorReading>("test late")
        {
        });
        sideOutput.print("test late");

        env.execute();
    }

}
