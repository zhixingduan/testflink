/**
 * 
 */
package com.duanzhixing.flink.other.window;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import com.duanzhixing.flink.other.window.AddFunction.Mysource;

/**
 * @author duanzhixing
 * 
 * 全量聚合
 * 等属于窗口的数据到齐，才开始进行聚合计算【可以实现对窗口内的数据进行排序等需求】
apply(windowFunction)
process(processWindowFunction)
processWindowFunction比windowFunction提供了更多的上下文信息。

 *
 */
public class FullAggregate
{

    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        SingleOutputStreamOperator<Tuple2<String, Integer>> map = env.addSource(new Mysource()).map(new MapFunction<String, Tuple2<String, Integer>>()
        {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception
            {
                return new Tuple2<>(value, 1);
            }
        });

        //process方法，每个窗口只执行一次,如下是等待3s内所有的tuple 都到齐了再进行count求和
        map.keyBy(0).timeWindow(Time.seconds(4)).process(new ProcessWindowFunction<Tuple2<String, Integer>, Tuple2<String, Integer>, Tuple, TimeWindow>()
        {

            @Override
            public void process(Tuple key, ProcessWindowFunction<Tuple2<String, Integer>, Tuple2<String, Integer>, Tuple, TimeWindow>.Context context, Iterable<Tuple2<String, Integer>> elements, Collector<Tuple2<String, Integer>> out) throws Exception
            {
                int count = 0;
                String key1 = "";
                for (Tuple2<String, Integer> tuple : elements)
                {
                    count += tuple.f1;

                    key1 = tuple.f0;
                }

                out.collect(new Tuple2<String, Integer>(key1, count));
            }

        }).print("process");

        env.execute();

    }

}
